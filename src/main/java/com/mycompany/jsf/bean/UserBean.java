package com.mycompany.jsf.bean;

import com.mycompany.jsf.entity.User;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.servlet.http.Part;
import lombok.Getter;
import lombok.Setter;

@ManagedBean(name = "userBean")
@SessionScoped
public class UserBean {

    @Getter
    @Setter
    private String name;

    @Getter
    @Setter
    private List<User> values;

    @Getter
    @Setter
    private List<User> items;

    @Getter
    @Setter
    private Part file;

    @PostConstruct
    public void init() {
        values = new ArrayList<>();
        items = new ArrayList<>();

        User u1 = new User();
        User u2 = new User();
        u1.setId(1);
        u1.setName("Antonio");

        u2.setId(2);
        u2.setName("Alexis");

        items.add(u1);
        items.add(u2);

    }

    public String pagina02() {
        return "/elementos/pagina02";
    }

    public String saludo() {
        return "Hola " + name;
    }

    public String forwardPage() {
        if ("2".equals(name)) {
            return "pagina02?faces-redirect=true";
        }

        return "pagina02";
    }

    public void uploadFile() {
        try {
            InputStream input = file.getInputStream();
            File f = new File("C:\\Users\\User\\Desktop\\foto\\file5x.jpg");

            if (!f.exists()) {
                f.createNewFile();
            }
            FileOutputStream output = new FileOutputStream(f);
            byte[] buffer = new byte[1024];
            int length;

            while ((length = input.read(buffer)) > 0) {
                output.write(buffer, 0, length);
            }
            
            input.close();
            output.close();
        } catch (Exception e) {
            
        }
    }

}
