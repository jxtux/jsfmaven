package com.mycompany.jsf.bean;

import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import lombok.Getter;
import lombok.Setter;

@ManagedBean(name = "messageBean")
@RequestScoped
public class MessageBean {

    @Getter
    @Setter
    private String input;

    public void definirMessage() {
        FacesContext context = FacesContext.getCurrentInstance();
        
        if ("1".equals(input)) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info Message", "detail..."));
        } else if ("2".equals(input)) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning Message", "detail..."));
        } else if ("3".equals(input)) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Fatal Message", "detail..."));
        } else if ("4".equals(input)) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error Message", "detail..."));
        }
    }

}
