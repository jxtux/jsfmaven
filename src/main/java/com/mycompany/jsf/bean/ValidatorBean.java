/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jsf.bean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import lombok.Getter;
import lombok.Setter;

@ManagedBean(name = "validatorBean")
@RequestScoped
public class ValidatorBean {

    @Getter
    @Setter
    private String username;

    @Getter
    @Setter
    private String output;

    public void sayUsername() {
        output = "hola " + this.username;
    }

}
