/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jsf.bean;

import com.mycompany.jsf.entity.User;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import lombok.Getter;
import lombok.Setter;

@ManagedBean(name = "tableBean")
@SessionScoped
public class tableBean {

    @Getter
    @Setter
    private List<User> users = null;

    @PostConstruct
    public void init() {
        if (users == null) {
            users = new ArrayList<User>();
        }
        users.add(new User("Murad", "Imanbayli", 25, false));
        users.add(new User("Fuad", "Pashabayli", 28, false));
        users.add(new User("Ilkin", "Abdullayev", 24, false));
        users.add(new User("Elmar", "Mammadov", 20, false));
    }

    public void delete() {
        for (int i = 0; i < users.size(); i++) {
            if (users.get(i).isSelected()) {
                users.remove(i);
                i--;
            }
        }
    }
}
