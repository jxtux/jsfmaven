package com.mycompany.jsf.bean;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.Part;
import lombok.Getter;
import lombok.Setter;

@ManagedBean(name = "uploadBean")
@SessionScoped
public class UploadBean {

    @Getter
    @Setter
    private Part image;

    @Getter
    @Setter
    private boolean upladed;

    public void doUpload() {
        try {
            InputStream in = image.getInputStream();

            File f = new File("C:\\Users\\User\\Desktop\\foto\\" + image.getSubmittedFileName());
            f.createNewFile();
            FileOutputStream out = new FileOutputStream(f);

            byte[] buffer = new byte[1024];
            int length;

            while ((length = in.read(buffer)) > 0) {
                out.write(buffer, 0, length);
            }

            out.close();
            in.close();

            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("path", f.getAbsolutePath());
            upladed = true;
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }

    }
}
