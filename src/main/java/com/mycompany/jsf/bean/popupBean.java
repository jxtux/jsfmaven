package com.mycompany.jsf.bean;

import java.io.Serializable;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;
import lombok.Getter;
import lombok.Setter;

@ManagedBean(name = "popupBean")
@RequestScoped
public class popupBean implements Serializable {

    @Getter
    @Setter
    private boolean showPopup;

    public void show() {
        showPopup = true;
    }

    public void hide() {
        showPopup = false;
    }

}
