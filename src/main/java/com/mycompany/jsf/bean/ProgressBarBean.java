package com.mycompany.jsf.bean;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import lombok.Getter;
import lombok.Setter;

@ManagedBean(name = "progressBarBean")
@SessionScoped
public class ProgressBarBean {

    @Getter
    @Setter
    private int progress;

    public void doProgress() {
        class ProgressThread extends Thread {

            @Override
            public void run() {
                while (progress < 100) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(ProgressBarBean.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    progress = progress + 5;
                }
            }

        }

        ProgressThread pt = new ProgressThread();
        pt.start();
    }

}
