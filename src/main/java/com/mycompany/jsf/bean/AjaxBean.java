package com.mycompany.jsf.bean;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import lombok.Getter;
import lombok.Setter;

@ManagedBean(name = "ajaxBean")
@RequestScoped
public class AjaxBean {

    @Getter
    @Setter
    String evento;

    @Getter
    @Setter
    String name;

    @Getter
    @Setter
    String outputText;

    @Getter
    @Setter
    String time;

    @Getter
    @Setter
    String resultText;

    public void showTime() {
        Date d = new Date();
        SimpleDateFormat df = new SimpleDateFormat("hh:mm:ss");
        time = df.format(d);
    }

    public void sendAction() {
        try {
            Thread.sleep(3000);
        } catch (Exception e) {
            Logger.getLogger(AjaxBean.class.getName()).log(Level.SEVERE, null, e);
        }
        
        resultText = "Success operacion "+new Date();
    }

    public void definirSaludo() {
        outputText = "hola " + name;
    }

    public void ajaxClick() {
        evento = "Ajax click event \n ";
    }

    public void ajaxChange() {
        evento = "Ajax change event \n ";
    }

    public void ajaxDoubleClick() {
        evento = "Ajax double click event \n ";
    }

    public void ajaxFocus() {
        evento = "Ajax focus event \n ";
    }

    public void ajaxkeyDown() {
        evento = "Ajax keydown event \n ";
    }

    public void ajaxkeyup() {
        evento = "Ajax keyup event \n ";
    }

    public void ajaxkeypress() {
        evento = "Ajax keypress event \n ";
    }

    public void ajaxMouseDown() {
        evento = "Ajax mouse down  event \n ";
    }

    public void ajaxMouseMove() {
        evento = "Ajax mouse move event \n ";
    }

    public void ajaxMouseOut() {
        evento = "Ajax mouse out event \n ";
    }

    public void ajaxMouseOver() {
        evento = "Ajax mouse over event \n ";
    }

    public void ajaxMouseUp() {
        evento = "Ajax Mouse Up event \n ";
    }

    public void ajaxSelect() {
        evento = "Ajax Select Event \n ";
    }

    public void ajaxBlur() {
        evento = "Ajax blur Event \n ";
    }

}
