package com.mycompany.jsf.bean;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import lombok.Getter;
import lombok.Setter;

@ManagedBean(name = "search")
@RequestScoped
public class searchBean {

    @Getter
    @Setter
    private List<String> data = null;

    @Getter
    @Setter
    private List<String> result = null;

    @Getter
    @Setter
    private String search = "";

    @PostConstruct
    public void init() {
        data = new ArrayList<String>();
        result = new ArrayList<String>();

        data.add("Antigua and Barbuda");
        data.add("Argentina");
        data.add("Aruba");
        data.add("Afghanistan");
        data.add("Azerbaijan");
    }

    public void search() {
        result.clear();
        for (String country : data) {
            if (country.toLowerCase().startsWith(search.toLowerCase())) {
                result.add(country);
            }
        }

        if (result.isEmpty()) {
            result.add("Not Found");
        }
    }

}
