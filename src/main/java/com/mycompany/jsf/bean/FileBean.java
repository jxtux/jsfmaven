package com.mycompany.jsf.bean;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

@ManagedBean(name = "fileBean")
@SessionScoped
public class FileBean {

    public void donwload() {
        try {
            FacesContext context = FacesContext.getCurrentInstance();
            ExternalContext externalContext = context.getExternalContext();

            externalContext.responseReset();
            externalContext.setResponseContentType("image/jpg");
            externalContext.setResponseHeader("Content-Disposition", "attachment;filename=\"image.jpg\"");

            FileInputStream inputStream = new FileInputStream(new File("C:\\Users\\User\\Desktop\\foto\\file5x.jpg"));
            OutputStream outputStream = externalContext.getResponseOutputStream();

            byte[] buffer = new byte[1024];
            int length;
            while ((length = inputStream.read(buffer)) > 0) {
                outputStream.write(buffer, 0, length);
            }

            inputStream.close();
            context.responseComplete();

        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
    }

}
