package com.mycompany.jsf.bean;

import com.google.gson.Gson;
import com.mycompany.jsf.entity.Citie;
import com.mycompany.jsf.entity.Contrie;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import lombok.Getter;
import lombok.Setter;

@ManagedBean(name = "eventBean")
@SessionScoped
public class EventBean {

    @Getter
    @Setter
    private List<Contrie> contries;

    @Getter
    @Setter
    private List<Citie> cities;

    @Getter
    @Setter
    private String value;

    @Getter
    @Setter
    private String fullName;

    @Getter
    @Setter
    private boolean editable;

    public void changeLocale(String lang) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.getViewRoot().setLocale(new Locale(lang));
    }

    @PostConstruct
    public void init() {
        contries = new ArrayList<>();
        cities = new ArrayList<>();

        Contrie c1 = new Contrie();
        c1.setId(1);
        c1.setName("Peru");

        Contrie c2 = new Contrie();
        c2.setId(2);
        c2.setName("Inglaterra");

        value = "data";
        contries.add(c1);
        contries.add(c2);

        fullName = "Jose antonio";
    }

    public void update() {
        editable = true;
    }

    public void save() {
        editable = false;
    }

    public void valueChange(ValueChangeEvent e) {
        value = "Old value:" + e.getOldValue() + ", New Value:" + e.getNewValue();

        if ("1".equals(e.getNewValue())) {
            cities.clear();
            Citie ci1 = new Citie();
            ci1.setId(1);
            ci1.setName("Lima");

            Citie ci2 = new Citie();
            ci2.setId(2);
            ci2.setName("Cuzco");

            cities.add(ci1);
            cities.add(ci2);
        } else if ("2".equals(e.getNewValue())) {
            cities.clear();
            Citie ci5 = new Citie();
            ci5.setId(1);
            ci5.setName("London");

            Citie ci6 = new Citie();
            ci6.setId(2);
            ci6.setName("Liverpol");

            cities.add(ci5);
            cities.add(ci6);
        }
    }

}
