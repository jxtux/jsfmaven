package com.mycompany.jsf.bean;

import com.mycompany.jsf.entity.Experience;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import lombok.Getter;
import lombok.Setter;

@ManagedBean(name = "dinamycBean")
@SessionScoped
public class dinamycBean {

    @Getter
    @Setter
    private List<Experience> experienceGroup = null;

    @Getter
    @Setter
    private String buttonValue = "No";

    @PostConstruct
    public void init() {
        experienceGroup = new ArrayList<Experience>();
    }

    public void add() {
        experienceGroup.add(new Experience());
    }

    public void remove(Experience e) {
        experienceGroup.remove(e);
    }

    public String save() {
        return "result.xhtml?faces-redirect=true";
    }

    public void changeButtonValue() {
        if ("No".equals(buttonValue)) {
            buttonValue = "Yes";
        } else {
            buttonValue = "No";
        }
    }
}
