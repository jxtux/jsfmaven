
package com.mycompany.jsf.bean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author User
 */
@ManagedBean(name = "hello_world")
@RequestScoped
public class HelloWorld {
   
    private String name;

    @PostConstruct
    public void inicializar(){
        name= "Igresar nombre";
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }   
    
    public HelloWorld() {
    }
    
}
