package com.mycompany.jsf.entity;

public class Experience {

    private String company;
    private String position;

    public Experience() {
    }

    public Experience(String company, String position) {
        this.company = company;
        this.position = position;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }
}
